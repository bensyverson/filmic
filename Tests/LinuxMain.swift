import XCTest

import FilmicTests

var tests = [XCTestCaseEntry]()
tests += FilmicTests.allTests()
XCTMain(tests)
