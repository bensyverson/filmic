//
//  File.swift
//  
//
//  Created by Ben Syverson on 2021/02/28.
//

import XCTest
import simd

extension Array where Element == Double {
	var vectors: [simd_double3] {
		map { simd_double3($0, $0, $0) }
	}
}

func assertEqual(_ left: [simd_double3], _ right: [simd_double3], tolerance: Double = 0.000000001) {
	zip(left, right).forEach {
		assertEqual($0.0, $0.1, tolerance: tolerance)
	}
}

func assertEqual(_ left: [Double], _ right: [Double], tolerance: Double = 0.000000001) {
	zip(left, right).forEach {
		assertEqual($0.0, $0.1, tolerance: tolerance)
	}
}

func assertEqual(_ left: simd_double3, _ right: simd_double3, tolerance: Double = 0.000000001) {
	XCTAssertLessThan(abs(left.x - right.x), tolerance)
	XCTAssertLessThan(abs(left.y - right.y), tolerance)
	XCTAssertLessThan(abs(left.z - right.z), tolerance)
}

func assertEqual(_ left: Double, _ right: Double, tolerance: Double = 0.000000001) {
	XCTAssertLessThan(abs(left - right), tolerance)
}
