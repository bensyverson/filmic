import XCTest
@testable import Filmic
import simd

final class FilmicTests: XCTestCase {
	let maxFour = [0.0, 0.5, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0]
	let toeTest = [ 0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5]

	func testLinearClampingDefault() {
		let filmic = Filmic()
		let stops = [0.0, 0.1, 0.2, 0.4, 0.8, 1.6, 3.2]
		let result = stops.map{ filmic.tonemap($0) }

		let expected = [0.0, 0.1, 0.2, 0.4, 0.8, 1.0, 1.0]
		assertEqual(result, expected)

		let roundTrip = result.map { filmic.inverseTonemap($0) }
		let expectedOut = [0.0, 0.1, 0.2, 0.4, 0.8, 1.4142135623730951, 1.4142135623730951]
		assertEqual(roundTrip, expectedOut)

		let stopVec = stops.vectors
		let resultVec = stopVec.map { filmic.tonemap($0) }

		let expectedVec = expected.vectors
		assertEqual(resultVec, expectedVec)

		let roundTripVec = resultVec.map { filmic.inverseTonemap($0) }
		let expectedOutVec = expectedOut.vectors
		assertEqual(roundTripVec, expectedOutVec)
	}

	func testModerateShoulder() {
		let filmic = Filmic(parameters: .init(toeStrength: 0.0,
											  toeLength: 0.5,
											  shoulderStrength: 0.5,
											  shoulderLength: 2.0,
											  shoulderAngle: 0.0,
											  gamma: 1.0))
		let expected = [
			0.0,
			0.5,
			0.9627096383653707,
			0.9933591055785206,
			0.9992820063688772,
			0.9999687772835572,
			0.9999998532038066,
			1.0,
		]
		let result = maxFour.map{ filmic.tonemap($0) }
		assertEqual(result, expected)
		let roundTrip = result.map { filmic.inverseTonemap($0) }
		assertEqual(roundTrip, maxFour)

		let resultVec = maxFour.vectors.map{ filmic.tonemap($0) }
		assertEqual(resultVec, expected.vectors)
		let roundTripVec = resultVec.map { filmic.inverseTonemap($0) }
		assertEqual(roundTripVec, maxFour.vectors)
	}

	func testSoftShoulder() {
		let filmic = Filmic(parameters:
								.init(toeStrength: 0.0,
									  toeLength: 0.5,
									  shoulderStrength: 0.9,
									  shoulderLength: 2.0,
									  shoulderAngle: 0.5,
									  gamma: 1.0))

		let expected = [
			0.0,
			0.3808238263978846,
			0.7957743782822383,
			0.8918860081967392,
			0.9471904305847487,
			0.9773456812165151,
			0.9927470389380432,
			1.0,
		]
		let result = maxFour.map{ filmic.tonemap($0) }
		assertEqual(result, expected)
		let roundTrip = result.map { filmic.inverseTonemap($0) }
		assertEqual(roundTrip, maxFour)

		let resultVec = maxFour.vectors.map{ filmic.tonemap($0) }
		assertEqual(resultVec, expected.vectors)
		let roundTripVec = resultVec.map { filmic.inverseTonemap($0) }
		assertEqual(roundTripVec, maxFour.vectors)
	}

	func testSoftToe() {
		let filmic = Filmic(parameters: .init(toeStrength: 0.5,
											  toeLength: 0.5,
											  shoulderStrength: 0.0,
											  shoulderLength: 0.5,
											  shoulderAngle: 0.0,
											  gamma: 1.0))

		let expected = [0.0, 0.011486983549970365, 0.04594793419988145, 0.09559058979399225, 0.14559058979399225, 0.19559058979399227, 0.24559058979399223, 0.2955905897939922, 0.34559058979399226, 0.39559058979399225, 0.44559058979399224]

		let result = toeTest.map{ filmic.tonemap($0) }
		assertEqual(result, expected)
		let roundTrip = result.map { filmic.inverseTonemap($0) }
		assertEqual(roundTrip, toeTest)

		let resultVec = toeTest.vectors.map{ filmic.tonemap($0) }
		assertEqual(resultVec, expected.vectors)
		let roundTripVec = resultVec.map { filmic.inverseTonemap($0) }
		assertEqual(roundTripVec, toeTest.vectors)
	}

	func testFilmicCurve() {
		let filmic = Filmic(parameters: .init(toeStrength: 0.4,
											  toeLength: 0.7,
											  shoulderStrength: 0.7,
											  shoulderLength: 2,
											  shoulderAngle: 1.0,
											  gamma: 1.0))

		let expected = [0.0, 0.3045361476793963, 0.7748680954368473, 0.8799415280123558, 0.9399505562326769, 0.9729422522609401, 0.9903063582179112, 0.9989957942195057]

		let result = maxFour.map{ filmic.tonemap($0) }
		assertEqual(result, expected)
		let roundTrip = result.map { filmic.inverseTonemap($0) }
		assertEqual(roundTrip, maxFour, tolerance: 1e-4)

		let resultVec = maxFour.vectors.map{ filmic.tonemap($0) }
		assertEqual(resultVec, expected.vectors)
		let roundTripVec = resultVec.map { filmic.inverseTonemap($0) }
		assertEqual(roundTripVec, maxFour.vectors, tolerance: 1e-4)
	}

	static var allTests = [
		("testLinearClampingDefault", testLinearClampingDefault),
		("testModerateShoulder", testModerateShoulder),
		("testSoftShoulder", testSoftShoulder),
		("testSoftToe", testSoftToe),
		("testFilmicCurve", testFilmicCurve),
	]
}
