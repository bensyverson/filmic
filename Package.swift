// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
	name: "Filmic",
	products: [
		.library(
			name: "Filmic",
			targets: ["Filmic"]),
	],
	dependencies: [],
	targets: [
		.target(
			name: "Filmic",
			dependencies: []),
		.testTarget(
			name: "FilmicTests",
			dependencies: ["Filmic"]),
	]
)
