//
//  File.swift
//  
//
//  Created by Ben Syverson on 2021/02/28.
//

import Foundation

internal extension Filmic {
	mutating func recalculateFromUserParameters() {
		var vars = parameters.variables
		w = vars.w
		invW = 1.0 / vars.w

		vars.x0 /= vars.w
		vars.x1 /= vars.w
		vars.overshootX = vars.overshootX / w

		// Linear
		let (m, b) = vars.slopeIntercept
		let g = vars.gamma

		linear = Segment(offsetX: -(b / m),
						 offsetY: 0.0,
						 scaleX: 1.0,
						 scaleY: 1.0,
						 lnA: g * log(max(m, 1e-5)),
						 b: g)

		let toeM = derivativeLinearGamma(m: m, b: b, gamma: g, x: vars.x0)
		let shoulderM = derivativeLinearGamma(m: m, b: b, gamma: g, x: vars.x1)

		vars.y0 = max(1e-5, pow(vars.y0, vars.gamma))
		vars.y1 = max(1e-5, pow(vars.y1, vars.gamma))

		vars.overshootY = pow(1 + vars.overshootY, vars.gamma) - 1

		x0 = vars.x0
		x1 = vars.x1
		y0 = vars.y0
		y1 = vars.y1

		// Toe section
		let (toeLnA, toeB) = solveAB(x0: vars.x0, y0: vars.y0, m: toeM)
		toe = Segment(lnA: toeLnA, b: toeB)

		// Shoulder section
		let shoulderX0 = (1 + vars.overshootX) - vars.x1
		let shoulderY0 = (1 + vars.overshootY) - vars.y1

		let (shoulderLnA, shoulderB) = solveAB(x0: shoulderX0, y0: shoulderY0, m: shoulderM)

		shoulder = Segment(
			offsetX: 1 + vars.overshootX,
			offsetY: 1 + vars.overshootY,
			scaleX: -1,
			scaleY: -1,
			lnA: shoulderLnA,
			b: shoulderB)

		let invScale = 1 / shoulder.eval(x: 1)
		toe.offsetY *= invScale
		toe.scaleY *= invScale

		linear.offsetY *= invScale
		linear.scaleY *= invScale

		shoulder.offsetY *= invScale
		shoulder.scaleY *= invScale
	}

	/// f(x) = (mx+b)^g
	/// f'(x) = gm(mx+b)^(g-1)
	private func derivativeLinearGamma(m: Double, b: Double, gamma g: Double, x: Double) -> Double {
		g * m * pow(m * x + b, g - 1)
	}

	/// find a function of the form:
	///   f(x) = e^(lnA + Bln(x))
	/// where
	///   f(0)   = 0; not really a constraint
	///   f(x0)  = y0
	///   f'(x0) = m
	private func solveAB(x0: Double, y0: Double, m: Double) -> (lnA: Double, b: Double) {
		let b = (m * x0) / y0
		let lnA = log(max(y0, 1e-5)) - b * log(max(x0, 1e-5))
		return (lnA, b)
	}
}
