//
//  File 2.swift
//  
//
//  Created by Ben Syverson on 2021/02/28.
//

extension Filmic {
	struct Variables {
		var x0: Double = 0.25
		var y0: Double = 0.25
		var x1: Double = 0.75
		var y1: Double = 0.75
		var w: Double = 1.0
		var gamma: Double = 1.0
		var overshootX: Double = 0.0
		var overshootY: Double = 0.0

		var slopeIntercept: (m: Double, b: Double) {
			let dy = y1 - y0
			let dx = x1 - x0
			let m = dx == 0 ? 1 : dy / dx
			let b = y0 - (x0 * m)
			return (m, b)
		}
	}
}
