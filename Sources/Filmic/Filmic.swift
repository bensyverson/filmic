import Foundation
import simd

/// Implements John Hable’s 2017 “Filmic Tone Curve”
public struct Filmic {
	/// User editable parameters
	public var parameters = Parameters() {
		didSet {
			recalculateFromUserParameters()
		}
	}

	public var toe = Segment()
	public var linear = Segment()
	public var shoulder = Segment()

	public var x0 = 0.25
	public var y0 = 0.25
	public var x1 = 0.75
	public var y1 = 0.75
	public var w = 1.0
	public var invW = 1.0

	/// Apply the Filmic tonemap to the HDR input value
	public func tonemap(_ x: Double) -> Double {
		let normX = x * invW
		return normX < x0 ?
			toe.eval(x: normX) :
			(normX < x1 ?
				linear.eval(x: normX) :
				shoulder.eval(x: normX)
			)
	}

	/// Apply the Filmic tonemap to the HDR input vector
	public func tonemap(_ x: simd_double3) -> simd_double3 {
		let normX = x * invW

		return .zero
			.replacing(with: toe.eval(x: normX), where: normX .< x0)
			.replacing(with: linear.eval(x: normX), where: normX .>= x0)
			.replacing(with: shoulder.eval(x: normX), where: normX .> x1)
	}

	/// Return the original HDR value for the tonemapped input
	public func inverseTonemap(_ y: Double) -> Double {
		w * (y < y0 ?
				toe.evalInv(y: y) :
				(y < y1 ?
					linear.evalInv(y: y) :
					shoulder.evalInv(y: y)
				))
	}

	/// Return the original HDR value for the tonemapped input vector
	public func inverseTonemap(_ y: simd_double3) -> simd_double3 {
		return (.zero
					.replacing(with: toe.evalInv(y: y), where: y .< y0)
					.replacing(with: linear.evalInv(y: y), where: y .>= y0)
					.replacing(with: shoulder.evalInv(y: y), where: y .>= y1)) * w
	}

	public init(parameters: Parameters = Parameters()) {
		self.parameters = parameters
		recalculateFromUserParameters()
	}
}
