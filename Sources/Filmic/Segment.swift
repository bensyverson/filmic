//
//  File.swift
//  
//
//  Created by Ben Syverson on 2021/02/28.
//

import Foundation
import simd

public extension Filmic {
	struct Segment {
		public var offsetX = 0.0
		public var offsetY = 0.0
		/// always initially 1 or -1
		public var scaleX = 1.0
		/// always initially 1 or -1
		public var scaleY = 1.0
		public var lnA = 0.0
		public var b = 1.0

		public func eval(x: Double) -> Double {
			let x0 = max((x - offsetX) * scaleX, 0)
			let y0 = exp(lnA + (b * log(x0)))
			return (y0 * scaleY) + offsetY
		}

		public func evalInv(y: Double) -> Double {
			let y0 = max((y - offsetY) / scaleY, 0)
			let x0 = exp((log(y0) - lnA) / b)
			return (x0 / scaleX) + offsetX
		}

		public func eval(x: simd_double3) -> simd_double3 {
			let x0 = max((x - offsetX) * scaleX, 0)

			let x0Log = simd_double3(log(x0.x), log(x0.y), log(x0.z))
			let withSlope: simd_double3 = lnA + (b * x0Log)
			let y0 = simd_double3(exp(withSlope.x), exp(withSlope.y), exp(withSlope.z))

			return (y0 * scaleY) + offsetY
		}

		public func evalInv(y: simd_double3) -> simd_double3 {
			let y0 = max((y - offsetY) / scaleY, 0)

			let y0Log = simd_double3(log(y0.x), log(y0.y), log(y0.z))
			let withSlope = (y0Log - lnA) / b
			let x0 = simd_double3(exp(withSlope.x), exp(withSlope.y), exp(withSlope.z))

			return (x0 / scaleX) + offsetX
		}
	}
}
