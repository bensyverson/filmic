//
//  File.swift
//  
//
//  Created by Ben Syverson on 2021/02/28.
//

import Foundation

extension Filmic.Parameters {
	/// Return "direct" parameters for the curve based on "User" parameters
	var variables: Filmic.Variables {
		var d = Filmic.Variables()
		let p = clamped

		// toe goes from 0 to 0.5
		d.x0 = p.toeLength * 0.5
		d.y0 = (1.0 - p.toeStrength) * d.x0 // lerp from 0 to x0

		let remainingY = 1.0 - d.y0

		let initialW = d.x0 + remainingY

		let y1Offset = (1.0 - p.shoulderStrength) * remainingY
		d.x1 = d.x0 + y1Offset
		d.y1 = d.y0 + y1Offset

		// filmic shoulder strength is in F stops
		let extraW = exp2(p.shoulderLength) - 1.0

		d.w = initialW + extraW

		// bake the linear to gamma space conversion
		d.gamma = p.gamma

		d.overshootX = (d.w * 2.0) * p.shoulderAngle * p.shoulderStrength
		d.overshootY = 0.5 * p.shoulderAngle * p.shoulderStrength
		return d
	}
}
