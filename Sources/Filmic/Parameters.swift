//
//  File.swift
//  
//
//  Created by Ben Syverson on 2021/02/28.
//

import Foundation

public extension Filmic {
	struct Parameters: Equatable & Codable {
		/// Amount of Toe to apply. 0.0 is linear, 1.0 crushes the length of the toe completely.
		public var toeStrength: Double = 0.0
		/// Where the toe section ends, in perceptual space.
		public var toeLength: Double = 0.5
		/// Amount of shoulder to apply.
		public var shoulderStrength: Double = 0.0
		/// The total latitude above 1.0, in F-stops. For example, to tonemap a maximum value of 4.0 in linear space, this parameter should be 2.0
		public var shoulderLength: Double = 0.5
		/// Lower values lead to a more faithful linear section, while higher values start rounding off earlier
		public var shoulderAngle: Double = 0.0
		/// The output gamma for this operation
		public var gamma: Double = 1.0

		private static let perceptualGamma = 2.2

		public var clamped: Parameters {
			Parameters(
				toeStrength: toeStrength.clamped(0, 1),
				toeLength: pow(toeLength.clamped(0, 1), Parameters.perceptualGamma),
				shoulderStrength: shoulderStrength.clamped(1e-5, 1),
				shoulderLength: max(0.0, shoulderLength),
				shoulderAngle: shoulderAngle.clamped(0, 1),
				gamma: gamma
			)
		}

		public init(toeStrength: Double = 0.0,
					toeLength: Double = 0.5,
					shoulderStrength: Double = 0.0,
					shoulderLength: Double = 0.5,
					shoulderAngle: Double = 0.0,
					gamma: Double = 1.0) {
			self.toeStrength = toeStrength
			self.toeLength = toeLength
			self.shoulderStrength = shoulderStrength
			self.shoulderLength = shoulderLength
			self.shoulderAngle = shoulderAngle
			self.gamma = gamma
		}
	}
}

private extension Comparable {
	func clamped(_ from: Self, _ to: Self) -> Self {
		min(max(self, from), to)
	}
}
