# Filmic

This package is a zero-dependency Swift implementation of [John Hable](http://filmicworlds.com)'s Filmic Tone Curve, using his original [2017 blog post](http://filmicworlds.com/blog/filmic-tonemapping-with-piecewise-power-curves/) and [C++ implementation](https://github.com/johnhable/fw-public) as guides.

This tone curve allows HDR imagery (values exceeding 1.0) to be "tonemapped" to SDR display range (0-1) or other HDR targets.

## Installation
To add this package to your project, simply add it as a dependency in your `Package.swift`, or add it via Xcode (File > Swift Packages > Add Package Dependency...)

```swift
...
	dependencies: [
		.package(
			url: "https://gitlab.com/bensyverson/filmic",
			from: "1.0"
		),
	],
	targets: [
		.target(
			name: "YourProject",
			dependencies: ["Filmic"]),
...

```

## Usage

Initialize a default/identity curve with `Filmic()`, then alter its `parameters` attribute, or pass those user parameters in directly to the initializer:

```swift
let filmic = Filmic(parameters:
	.init(toeStrength: 0.0,
		toeLength: 0.5,
		shoulderStrength: 0.9,
		shoulderLength: 2.0,
		shoulderAngle: 0.5,
		gamma: 1.0
	)
)
```

From there, the curve can be used to tonemap `Double` values or 3-element vectors from the [standard library](https://developer.apple.com/documentation/swift/simd): `SIMD3<Double>` (aka `simd_double3`).

```swift
let output: Double = filmic.tonemap(value: 2.8)

let rgbOut = filmic.tonemap(
	value: simd_double3(0,1,2)
)

```

## User Parameters

With the default parameters, the curve is linear, but will clamp values above 1.0. To create a tonemap, you will typically use John's artist-friendly "User Parameters."

### Shoulder Length (Latitude)

First determine the `shoulderLength`, which specifies the maximum white value in F-stops above 1.0. For example, if the maximum value in your image is 16.0 in linear encoding, `shoulderLength` should be **4.0**.

### Shoulder parameters
From there, `shoulderStrength` will control the sharpness of the shoulder, while `shoulderAngle` will affect the behavior of the curve near white.

### Toe parameters
You can set the length of the toe with `toeLength`, while `toeStrength` defines how much the toe is crushed. With `toeStrength` at 0.0, the toe is linear, at 0.5 it's a substantial curve, and at 1.0, the toe is crushed completely, and the linear section rises directly from black/0.0.

### Gamma
Finally, you can adjust the output gamma with `gamma`. The default value of 1.0 is linear, so the output can be passed to a separate [Tone Response Curve](https://en.wikipedia.org/wiki/Tone_reproduction) such as [sRGB](https://en.wikipedia.org/wiki/SRGB)'s transfer function. However, if a simpler display gamma is acceptable (such as 2.2, approximating sRGB), that gamma can be built into the Filmic curve directly.

## Support
This library is provided as-is. I'm not able to offer technical support, so Issues are disabled for this repo. However, if you find a bug or have an enhancement, PRs are welcome!

## License

John Hable's original [Filmic Tone Curve repository](https://github.com/johnhable/fw-public) is [licensed](https://github.com/johnhable/fw-public/blob/master/LICENSE.txt) under [Creative Commons CC0](https://creativecommons.org/share-your-work/public-domain/cc0/).

This package is available under the more standard [MIT License](LICENSE.md).
